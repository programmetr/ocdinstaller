package com.craftingdead.installer;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;

public class JsonGetter {

	private String obj1Path;
	// public String name;
	public String jarName;
	public long ver;
	// public String forgeVer;

	public String ZipName112;
	public String url112;
	public boolean option1Enabled;
	public String option1Text;
	public String ZipName164;
	public String url164;
	public boolean option2Enabled;
	public String option2Text;
	public long latestVer;
	public String latestCDJsonName;
	public boolean outdated = false;

	public JsonGetter(String obj1Path) {

		this.obj1Path = obj1Path;

		Object obj1 = null;

		if (Files.exists(Paths.get(obj1Path))) {

			try {
				obj1 = new JSONParser().parse(new FileReader(obj1Path));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}

			JSONObject jObj1 = (JSONObject) obj1;

			// name = (String) jObj1.get("name");
			jarName = (String) jObj1.get("jarName");
			ver = (long) jObj1.get("ver");
			// forgeVer = (String) jObj1.get("forgeVer");
		}

		System.out.println(jarName);

		Object obj2 = null;
		try {

			URL url = new URL("http://dl.brad.ac/uploads/craftingdead/reborn/dependants/launcher.json");
			URLConnection connURL = null;
			connURL = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(connURL.getInputStream()));

			String cnts;
			String contents = "";
			while ((cnts = in.readLine()) != null) {

				contents += cnts;

			}

			System.out.println(contents);

			obj2 = new JSONParser().parse(contents);

		} catch (IOException e) {
			e.printStackTrace();

		} catch (ParseException e) {

			JOptionPane.showMessageDialog(null, "Couldn't connect to dl.brad.ac/! The server might be offline or you're not connected to the internet!",
					"ERROR: OCD Installer", JOptionPane.ERROR_MESSAGE);

			e.printStackTrace();
		}

		JSONObject jObj2 = (JSONObject) obj2;

		ZipName112 = (String) jObj2.get("ZipName1.12.2");
		ZipName164 = (String) jObj2.get("ZipName1.6.4");
		latestVer = (long) jObj2.get("latestVer");
		url112 = (String) jObj2.get("url1.12.2");
		url164 = (String) jObj2.get("url1.6.4");
		option1Text = (String) jObj2.get("OptionOne");
		option2Text = (String) jObj2.get("OptionTwo");
		option1Enabled = (boolean) jObj2.get("optOneEnabled");
		option2Enabled = (boolean) jObj2.get("optTwoEnabled");
		latestCDJsonName = (String) jObj2.get("latestCDJsonName");

		if (ver == latestVer) {

			outdated = false;

		}

		else {

			outdated = true;

		}
	}

	/*if you uninstall it immediately after you have installed it it won't delete it the jar because the current jarname is null*/
	public void updateContents(){

		Object obj1 = null;
		try {
			obj1 = new JSONParser().parse(new FileReader(obj1Path));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		JSONObject jObj1 = (JSONObject) obj1;

		// name = (String) jObj1.get("name");
		jarName = (String) jObj1.get("jarName");
		ver = (long) jObj1.get("ver");

	}


	public void addLauncherProfile(String profilePath)throws IOException{

		Object objForge = null;
		try {
			objForge = new JSONParser().parse(new FileReader(profilePath));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		JSONObject jObjProf = (JSONObject) objForge;
		JSONObject jObjCD = new JSONObject();
		JSONObject jObjSet = new JSONObject();

		jObjSet.put("name", "craftingdead");
		jObjSet.put("type", "custom");
		jObjSet.put("created", "2018-09-10T15:18:56.115Z");
		jObjSet.put("lastUsed", "2018-11-02T22:45:55.888Z");
		jObjSet.put("lastVersionId", "craftingdead");

		jObjCD.put("craftingdead", jObjSet);

		JSONObject newProfs = (JSONObject) jObjProf.get("profiles");
		newProfs.putAll(jObjCD);

		jObjProf.put("profiles", newProfs);


		try (FileWriter file = new FileWriter(profilePath)) {
			file.write(jObjProf.toJSONString());
			System.out.println("Successfully Copied json Object to File...\n");
			System.out.println("json Object: " + jObjProf);
		}


	}

	public void removeLauncherProfile(String profilePath)throws IOException{

		Object objForge = null;
		try {
			objForge = new JSONParser().parse(new FileReader(profilePath));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		JSONObject jObjProf = (JSONObject) objForge;

		JSONObject newProfs = (JSONObject) jObjProf.get("profiles");


		newProfs.remove("craftingdead");


		try (FileWriter file = new FileWriter(profilePath)) {
			file.write(jObjProf.toJSONString());
			System.out.println("Successfully wrote json Object to File...\n");
			System.out.println("json Object: " + jObjProf);
		}


	}

}