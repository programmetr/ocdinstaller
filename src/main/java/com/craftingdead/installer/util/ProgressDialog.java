package com.craftingdead.installer.util;

import org.omg.PortableServer.THREAD_POLICY_ID;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;
import javax.swing.border.Border;

public class ProgressDialog {

	private JFrame frame;
	private JProgressBar bar;
	private String title;

	public ProgressDialog(String title, String message) {

		this.title = title;
		frame = new JFrame(title);
		frame.pack();
		frame.setSize(500, 75);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JOptionPane.showMessageDialog(frame, "Please wait for the installation to finish.", "Warning",
						JOptionPane.WARNING_MESSAGE);
			}
		});

		bar = new JProgressBar(0, 100);
		bar.setStringPainted(true);

		Border border = BorderFactory.createTitledBorder(message);
		bar.setBorder(border);

		Container content = frame.getContentPane();
		content.add(bar, BorderLayout.NORTH);

		frame.setVisible(true);

	}

	public void setTitle(String text){

		this.title = title;
		frame.setTitle(text);

	}

	public void setProgress(int percent) {

		this.bar.setValue(percent);

	}

	public void close() {

		this.frame.dispose();

	}

}
