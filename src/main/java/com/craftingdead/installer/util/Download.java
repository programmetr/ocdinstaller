package com.craftingdead.installer.util;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Observable;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Download extends Observable implements Callable<Void> {

	private static final int MAX_BUFFER_SIZE = 1024;

	public static final String STATUSES[] = { "Downloading", "Paused", "Complete", "Cancelled", "Error" };

	public static final int DOWNLOADING = 0;
	public static final int PAUSED = 1;
	public static final int COMPLETE = 2;
	public static final int CANCELLED = 3;
	public static final int ERROR = 4;

	private static ExecutorService executor = Executors.newCachedThreadPool();

	private URL url;
	private File location;
	private int size;
	private int downloaded;
	private int status;
	private File file;
	public static ProgressDialog dialog;

	public Download() {
		size = -1;
		downloaded = 0;
		dialog = new ProgressDialog("Downloading ", "Please Wait...");
	}

	public String getUrl() {
		return url.toString();
	}

	public int getSize() {
		return size;
	}

	public float getProgress() {
		return ((float) downloaded / size) * 100;
	}

	public void setDownloaded(int downloaded){

		this.downloaded = downloaded;
		this.getSize();

	}

	public int getStatus() {
		return status;
	}

	public void pause() {
		status = PAUSED;
		stateChanged();
	}

	public void setUrlFile(URL url, File file){

		this.url = url;
		this.file = file;
		this.location = file;

	}

	public Future<Void> start() {
		status = DOWNLOADING;
		stateChanged();
		return executor.submit(this);
	}

	public void cancel() {
		status = CANCELLED;
		stateChanged();
	}

	private void error() {
		status = ERROR;
		stateChanged();
	}

	@Override
	public Void call() {
		RandomAccessFile file = null;
		InputStream stream = null;

		try {
			URLConnection connection = url.openConnection();
			connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			connection.setRequestProperty("Range", "bytes=" + downloaded + "-");
			connection.connect();

			if (((HttpURLConnection) connection).getResponseCode() / 100 != 2) {
				error();
			}

			int contentLength = connection.getContentLength();
			if (contentLength < 1) {
				error();
			}

			if (size == -1) {
				size = contentLength;
				stateChanged();
			}

			file = new RandomAccessFile(this.location.getAbsolutePath(), "rw");
			file.seek(downloaded);

			stream = connection.getInputStream();
			while (status == DOWNLOADING) {
				byte buffer[];
				if (size - downloaded > MAX_BUFFER_SIZE) {
					buffer = new byte[MAX_BUFFER_SIZE];
				} else {
					buffer = new byte[size - downloaded];
				}

				int read = stream.read(buffer);
				if (read == -1)
					break;

				file.write(buffer, 0, read);
				downloaded += read;
				if(this.getProgress() >= 100.0){

					status = COMPLETE;
					System.out.println(this.getProgress());

				}
				stateChanged();
			}


		} catch (Exception e) {
			error();
		} finally {
			if (file != null) {
				try {
					file.close();
				} catch (Exception e) {
				}
			}

			if (stream != null) {
				try {
					stream.close();
				} catch (Exception e) {
				}
			}
		}
		return null;
	}

	private void stateChanged() {
		System.out.println("Downloading: " + url.toString() + " " + this.getProgress() + "%");
		dialog.setProgress((int) this.getProgress());
		dialog.setTitle("Downloading: " +  file.getName() + " - " + Math.round(this.getProgress()) + "%");
		setChanged();
		notifyObservers();
	}
}