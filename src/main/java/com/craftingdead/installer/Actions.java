package com.craftingdead.installer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.json.simple.JSONObject;

public class Actions {

	@SuppressWarnings("unchecked")
	public static void makeJsonFile(long ver, String path) throws IOException {

		if (Files.exists(Paths.get(path))){

			delelteFiles(Paths.get(path));

		}

		JSONObject obj = new JSONObject();
		obj.put("ver", ver);
		try (FileWriter file = new FileWriter(path)) {
			file.write(obj.toJSONString());
			System.out.println("Successfully Copied json Object to File...");
			System.out.println("\njson Object: " + obj);
		}

	}

	public static void deleteDirectoryStream(Path path) throws IOException {
		Files.walk(path).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
	}

	public static void move(String SourcePath, String DestPath) {
		try {
			Path path_dest = Paths.get(DestPath);
			if (Files.exists(path_dest)) {
				deleteDirectoryStream(path_dest);
				Files.move(Paths.get(SourcePath), Paths.get(DestPath));
			} else {
				Files.move(Paths.get(SourcePath), Paths.get(DestPath));
			}
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	// Only copies the folder without contents so I commented this out

	/*
	 * public void copyDir(String SourcePath, String DestPath){
	 * 
	 * Path srcPath = Paths.get(SourcePath); Path dstPath = Paths.get(DestPath);
	 * 
	 * try {
	 * 
	 * if(Files.exists(dstPath)){
	 * 
	 * deleteDirectoryStream(dstPath); Files.copy(srcPath, dstPath,
	 * StandardCopyOption.REPLACE_EXISTING);
	 * 
	 * } else{
	 * 
	 * Files.copy(srcPath, dstPath, StandardCopyOption.REPLACE_EXISTING);
	 * 
	 * } } catch (IOException e) {
	 * 
	 * e.printStackTrace();
	 * 
	 * } }
	 */

	public static void delelteFiles(Path path) {

		if (Files.exists(path)) {

			try {
				deleteDirectoryStream(path);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	public static void unZip(String src, String dest) {

		/*
		 * minecraft_path.getText() + "\\mods\\mod.zip" minecraft_path.getText() +
		 * "\\mods\\CraftingDead\\
		 */

		try (ZipFile file = new ZipFile(src)) {

			FileSystem fileSystem = FileSystems.getDefault();
			// Get file entries
			Enumeration<? extends ZipEntry> entries = file.entries();

			// We will unzip files in this folder
			String uncompressedDirectory = dest;
			Path path = Paths.get(dest);

			if (Files.exists(path)) {

				deleteDirectoryStream(path);
				Files.createDirectory(fileSystem.getPath(uncompressedDirectory));
			}

			else {

				Files.createDirectory(fileSystem.getPath(uncompressedDirectory));

			}

			// Iterate over entries
			while (entries.hasMoreElements()) {

				ZipEntry entry = entries.nextElement();
				// If directory then create a new directory in uncompressed folder

				if (entry.isDirectory()) {

					System.out.println("Creating Directory:" + uncompressedDirectory + entry.getName());
					Files.createDirectories(fileSystem.getPath(uncompressedDirectory + entry.getName()));

				}
				// Else create the file
				else {

					InputStream is = file.getInputStream(entry);
					BufferedInputStream bis = new BufferedInputStream(is);
					String uncompressedFileName = uncompressedDirectory + entry.getName();
					Path uncompressedFilePath = fileSystem.getPath(uncompressedFileName);
					Files.createDirectories(uncompressedFilePath.getParent());
					Files.createFile(uncompressedFilePath);
					FileOutputStream fileOutput = new FileOutputStream(uncompressedFileName);

					while (bis.available() > 0) {

						fileOutput.write(bis.read());

					}

					fileOutput.close();
					System.out.println("Written :" + entry.getName());
				}
			}
		} catch (IOException e) {

			e.printStackTrace();

		}
	}

}
