package com.craftingdead.installer;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.*;

import com.craftingdead.installer.util.Download;

import static sun.misc.PostVMInitHook.run;

@SuppressWarnings("serial")
public class Gui extends JPanel implements ActionListener {

    public JPanel topPanel = new JPanel();
    public JPanel checkPanel = new JPanel();
    public JPanel middlePanel = new JPanel();
    public JPanel botomPanel = new JPanel();

    public ImageIcon backgroundImage = new ImageIcon(getClass().getResource("/bg.png"));
    public JLabel background = new JLabel(backgroundImage);

    public JCheckBox chBox112 = new JCheckBox();
    public JCheckBox chBox164 = new JCheckBox();

    public JButton btnInstall = new JButton("Install Crafting Dead");
    public JLabel lblInstall = new JLabel("Installs the Crafting Dead modpack and Forge");

    public JButton btnUpdate = new JButton("Update Crafting Dead");
    public JLabel lblUpdate = new JLabel("Updates the Crafting Dead modpack");

    public JButton btnUninstall = new JButton("Uninstall Crafting Dead");
    public JLabel lblUninstall = new JLabel("Uninstalls the Crafting Dead modpack");

    public JButton btnAdvanced = new JButton("Advanced");
    public JButton btnCancel = new JButton("Cancel");

    public JButton btnAdd;
    public JButton installMCButton;

    public String os = System.getProperty("os.name");
    //public Path potCDPath;
    public Path potForgePath;
    public Path potJsonPath;
    public Path potMcPath;

    public JsonGetter jsonGetter;

    public String selectedUrl;
    public String selectedVer;
    public String selectedZipVer;

    public Gui() {

        if (os.toLowerCase().contains("windows")) {

            System.out.println("Windows!");

            potMcPath = Paths.get(System.getenv("APPDATA") + "\\.minecraft\\");

        }

        else if (os.toLowerCase().contains("mac")) {

            System.out.println("Mac!");

            potMcPath = Paths.get(System.getProperty("user.home") + "/Library/Application Support/minecraft/");

        }

        else if (os.toLowerCase().contains("linux")) {

            System.out.println("Linux!");
            potMcPath = Paths.get(System.getProperty("user.home") + "/.minecraft/");

        }

        else {

            JOptionPane.showMessageDialog(null, "Your os isn't really supported! Some features will not work!",
                    "Alert: OCD Installer", JOptionPane.INFORMATION_MESSAGE);

        }

        System.out.println(potMcPath.toString());
        potJsonPath = Paths.get(potMcPath.toString() + File.separator + "mods" + File.separator + "contents.json");
        jsonGetter = new JsonGetter(potJsonPath.toString());
        //potCDPath = Paths.get(potMcPath.toString() + File.separator + "mods" + File.separator + jsonGetter.jarName);
        potForgePath = Paths.get(potMcPath.toString() + File.separator + "versions" + File.separator + "craftingdead" + File.separator);

        chBox112.setText(jsonGetter.option1Text);
        chBox164.setText(jsonGetter.option2Text);

        if(!jsonGetter.option1Enabled) chBox112.setEnabled(false);
        if(!jsonGetter.option2Enabled) chBox164.setEnabled(false);

        System.out.println(potForgePath);

        if (Files.exists(potJsonPath) && Files.exists(potForgePath)) {

            btnInstall.setEnabled(false);
            btnUpdate.setEnabled(true);

        }

        else {

            btnInstall.setEnabled(true);
            btnUpdate.setEnabled(false);

        }

        btnInstall.addActionListener(this);
        btnUpdate.addActionListener(this);
        btnUninstall.addActionListener(this);
        btnAdvanced.addActionListener(this);
        btnCancel.addActionListener(this);

        topPanel.setLayout(new FlowLayout());
        checkPanel.setLayout(new GridLayout(1, 1));
        middlePanel.setLayout(new GridLayout(9, 1));
        botomPanel.setLayout(new FlowLayout());

        topPanel.add(background);

        checkPanel.add(chBox112);
        checkPanel.add(chBox164);

        middlePanel.add(checkPanel);

        middlePanel.add(btnInstall);
        middlePanel.add(lblInstall);
        middlePanel.add(btnUpdate);
        middlePanel.add(lblUpdate);
        middlePanel.add(btnUninstall);
        middlePanel.add(lblUninstall);

        botomPanel.add(btnAdvanced);
        botomPanel.add(btnCancel);
        middlePanel.add(botomPanel);

        setLayout(new FlowLayout());

        add(topPanel);
        add(middlePanel);

    }

    public void installModAndForge(URL url, File out, URL urlCDJsonUrl, File urlCDJsonOut) {

        File cdFolder = new File(potMcPath.toString() + File.separator + "craftingdead" + File.separator);
        cdFolder.mkdir();

        if(!Files.exists(Paths.get(potMcPath.toString() + File.separator + "mods" + File.separator))){

            File modsFolder = new File(potMcPath.toString() + File.separator + "mods" + File.separator);
            modsFolder.mkdir();

        }

        Download modDownloader = new Download();
        try {
            modDownloader.setUrlFile(url, out);
            modDownloader.start().get();
        } catch (InterruptedException | ExecutionException e1) {
            e1.printStackTrace();
        }

        try {
            modDownloader.setDownloaded(0);
            modDownloader.setUrlFile(urlCDJsonUrl, urlCDJsonOut);
            modDownloader.start().get();
        } catch (InterruptedException | ExecutionException e1) {
            e1.printStackTrace();
        }

        modDownloader.dialog.setTitle("Installing: Please wait...");

        Actions.unZip(potMcPath.toString() + File.separator + "mods" + File.separator + selectedZipVer,
                potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator);

        File cdModFolder = new File(potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator + "mods" + File.separator);

        FilenameFilter jarFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {

                return name.toLowerCase().endsWith(".jar");

            }
        };

        File[] jarFiles = cdModFolder.listFiles(jarFilter);

        System.out.println("Jar count: " + jarFiles.length);

        for(int i = 0; i < jarFiles.length; i++){

            Actions.move(potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator + "mods" + File.separator + jarFiles[i].getName(),
                    potMcPath.toString() + File.separator + "mods" + File.separator + jarFiles[i].getName());

        }


        if (Files.exists(potForgePath)) {

            System.out.println("Skipping forge!");

        }

        else {

            if(selectedVer.equals("1.12.2")){

                Actions.unZip("dep" + File.separator + "craftingdeadforge1-12-2.zip", potMcPath.toString() + File.separator + "versions" + File.separator + "craftingdead" + File.separator);
                Actions.unZip("dep" + File.separator + "minecraftforge1-12-2.zip", potMcPath.toString() + File.separator + "libraries" + File.separator + "net" + File.separator + "minecraftforge" + File.separator + File.separator + "minecraftforge" + File.separator);

            }

            else if(selectedVer.equals("1.6.4")){

                Actions.unZip("dep" + File.separator + "craftingdeadforge1-6-4.zip", potMcPath.toString() + File.separator + "versions" + File.separator + "craftingdead" + File.separator);
                Actions.unZip("dep" + File.separator + "minecraftforge1-6-4.zip", potMcPath.toString() + File.separator + "libraries" + File.separator + "net" + File.separator + "minecraftforge" + File.separator + File.separator + "minecraftforge" + File.separator);

            }

            try {
                jsonGetter.addLauncherProfile(potMcPath.toString() + File.separator + "launcher_profiles.json");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            Actions.makeJsonFile(jsonGetter.latestVer,
                    potMcPath.toString() + File.separator + "mods" + File.separator + "contents.json");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Download.dialog.close();

        JOptionPane.showMessageDialog(null, "Completed!", "Alert: OCD Installer", JOptionPane.INFORMATION_MESSAGE);

        jsonGetter.updateContents();
        jsonGetter.outdated = false;

        try {
            Actions.deleteDirectoryStream(Paths.get(potMcPath.toString() + File.separator + "mods" + File.separator + selectedZipVer));
        } catch (IOException e) {
            e.printStackTrace();
        }

        chBox164.setSelected(false);
        chBox112.setSelected(false);
        btnInstall.setEnabled(false);
        btnUpdate.setEnabled(false);
    }

    public void updateMod(URL url, File out, URL urlCDJsonUrl, File urlCDJsonOut) {

        File cdFolder = new File(potMcPath.toString() + File.separator + "craftingdead" + File.separator);
        cdFolder.mkdir();

        if(!Files.exists(Paths.get(potMcPath.toString() + File.separator + "mods" + File.separator))){

            File modsFolder = new File(potMcPath.toString() + File.separator + "mods" + File.separator);
            modsFolder.mkdir();

        }

        Download modDownloader = new Download();

        try {
            modDownloader.setUrlFile(url, out);
            modDownloader.start().get();
        } catch (InterruptedException | ExecutionException e1) {
            e1.printStackTrace();
        }

        try {
            modDownloader.setDownloaded(0);
            modDownloader.setUrlFile(urlCDJsonUrl, urlCDJsonOut);
            modDownloader.start().get();
        } catch (InterruptedException | ExecutionException e1) {
            e1.printStackTrace();
        }

        modDownloader.dialog.setTitle("Installing: Please wait...");

        Actions.unZip(potMcPath.toString() + File.separator + "mods" + File.separator + selectedZipVer,
                potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator);

        File cdModFolder = new File(potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator + "mods" + File.separator);

        FilenameFilter jarFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {

                return name.toLowerCase().endsWith(".jar");

            }
        };

        File[] jarFiles = cdModFolder.listFiles(jarFilter);

        System.out.println("Jar count: " + jarFiles.length);

        for(int i = 0; i < jarFiles.length; i++){

            Actions.move(potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator + "mods" + File.separator + jarFiles[i].getName(),
                    potMcPath.toString() + File.separator + "mods" + File.separator + jarFiles[i].getName());

        }

        try {
            Actions.makeJsonFile(jsonGetter.latestVer,
                    potMcPath.toString() + File.separator + "mods" + File.separator + "contents.json");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Download.dialog.close();

        JOptionPane.showMessageDialog(null, "Completed!", "Alert: OCD Installer", JOptionPane.INFORMATION_MESSAGE);

        jsonGetter.updateContents();
        jsonGetter.outdated = false;

        try {
            Actions.deleteDirectoryStream(Paths.get(potMcPath.toString() + File.separator + "mods" + File.separator + selectedZipVer));
        } catch (IOException e) {
            e.printStackTrace();
        }

        chBox164.setSelected(false);
        chBox112.setSelected(false);
        btnInstall.setEnabled(false);
        btnUpdate.setEnabled(false);
    }

    public void UninstallCDFiles() {

        List<String> paths = new ArrayList<String>();

        FilenameFilter jarFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {

                return name.toLowerCase().endsWith(".jar");

            }
        };

        File modsFolder = new File(potMcPath.toString() + File.separator + "mods" + File.separator);

        File[] jarFiles = modsFolder.listFiles(jarFilter);

        System.out.println("Jar count: " + jarFiles.length);

        for(int i = 0; i < jarFiles.length; i++){

            paths.add(potMcPath.toString() + File.separator + "mods" + File.separator + jarFiles[i].getName());

        }
        paths.add(potMcPath.toString() + File.separator + "mods" + File.separator + jsonGetter.jarName);
        paths.add(potMcPath.toString() + File.separator + "craftingdead" + File.separator);
        paths.add(potMcPath.toString() + File.separator + "mods" + File.separator + "contents.json");
        paths.add(potMcPath.toString() + File.separator + "mods" + File.separator + "CraftingDead" + File.separator);
        paths.add(potMcPath.toString() + File.separator + "versions" + File.separator + "craftingdead" + File.separator);

        for(String src: paths){

            Path srcPath = Paths.get(src);
            Actions.delelteFiles(srcPath);

        }

        try {
            jsonGetter.removeLauncherProfile(potMcPath + File.separator + "launcher_profiles.json");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        JOptionPane.showMessageDialog(null, "Successfully deleted CD!", "Alert: OCD Installer",
                JOptionPane.INFORMATION_MESSAGE);
        btnInstall.setEnabled(true);
        btnUpdate.setEnabled(false);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == btnInstall) {

            if (Files.exists(potMcPath)) {

                if(chBox112.isSelected()) {

                    selectedUrl = jsonGetter.url112;
                    selectedZipVer = jsonGetter.ZipName112;
                    selectedVer = "1.12.2";

                }
                if(chBox164.isSelected()) {

                    selectedUrl = jsonGetter.url164;
                    selectedZipVer = jsonGetter.ZipName164;
                    selectedVer = "1.6.4";

                }

                if(!chBox112.isSelected() && !chBox164.isSelected()){

                    JOptionPane.showMessageDialog(null, "Nothing selected!", "Alert: OCD Installer",
                            JOptionPane.ERROR_MESSAGE);

                    return;

                }

                JOptionPane.showMessageDialog(null, "Installing CD! This may take a while!",
                        "Installing: OCD Installer", JOptionPane.INFORMATION_MESSAGE);
                try {
                    installModAndForge(
                            new URL(selectedUrl + selectedZipVer),
                            new File(potMcPath.toString() + File.separator + "mods" + File.separator + selectedZipVer),
                            new URL("http://dl.brad.ac/uploads/craftingdead/reborn/dependants/" + jsonGetter.latestCDJsonName),
                            new File(potMcPath.toString() + File.separator + "craftingdead" + File.separator + jsonGetter.latestCDJsonName));
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }

            }

            else {

                JOptionPane.showMessageDialog(null,
                        "Minecraft Path incorrect! You can add it manually in Advanced Settings!",
                        "Error: OCD Installer", JOptionPane.ERROR_MESSAGE);

            }

        }

        else if (e.getSource() == btnUpdate) {

            if (Files.exists(potMcPath)) {

                if (jsonGetter.outdated) {

                    if(chBox112.isSelected()) {

                        selectedZipVer = jsonGetter.ZipName112;
                        selectedUrl = jsonGetter.url112;
                        selectedVer = "1.12";

                    }
                    if(chBox164.isSelected()) {

                        selectedZipVer = jsonGetter.ZipName164;
                        selectedUrl = jsonGetter.url164;
                        selectedVer = "1.6.4";

                    }

                    if(!chBox112.isSelected() && !chBox164.isSelected()){

                        JOptionPane.showMessageDialog(null, "Successfully deleted CD!", "Alert: OCD Installer",
                                JOptionPane.ERROR_MESSAGE);

                        return;

                    }

                    JOptionPane.showMessageDialog(null, "A newer version of CD has been found! Updating!",
                            "Alert: OCD Installer", JOptionPane.INFORMATION_MESSAGE);

                    try {
                        updateMod(
                                new URL(selectedUrl + selectedZipVer),
                                new File(potMcPath.toString() + File.separator + "mods" + File.separator + selectedZipVer),
                                new URL("http://dl.brad.ac/uploads/craftingdead/reborn/dependants/" + jsonGetter.latestCDJsonName),
                                new File(potMcPath.toString() + File.separator + "craftingdead" + File.separator + jsonGetter.latestCDJsonName));
                    } catch (MalformedURLException e1) {
                        e1.printStackTrace();
                    }

                } else {

                    JOptionPane.showMessageDialog(null, "Up to date!", "Alert: OCD Installer",
                            JOptionPane.INFORMATION_MESSAGE);

                }

            }

            else {

                JOptionPane.showMessageDialog(null,
                        "Minecraft Path incorrect! You can add it manually in Advanced Settings!",
                        "Error: OCD Installer", JOptionPane.ERROR_MESSAGE);

            }

        }

        else if (e.getSource() == btnUninstall) {

            if (Files.exists(potMcPath)) {

                int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete CD?",
                        "Alert: OCD Installer", JOptionPane.YES_NO_OPTION);

                if (answer == JOptionPane.YES_OPTION) {

                    UninstallCDFiles();

                }

                else {

                    return;

                }

            }

            else {

                JOptionPane.showMessageDialog(null,
                        "Minecraft Path incorrect! You can add it manually in Advanced Settings!",
                        "Error: OCD Installer", JOptionPane.ERROR_MESSAGE);

            }

        }

        else if (e.getSource() == btnAdvanced) {

            JFrame advancedFrame = new JFrame();
            advancedFrame.setTitle("Advanced Settings");
            advancedFrame.setResizable(false);
            advancedFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            JLabel mcLabel = new JLabel("  MC Path: ");
            JLabel installMCLabel = new JLabel("  Install MC: ");
            /*mcTextBox = new JTextField(15);*/
            btnAdd = new JButton("Add MC Path");
            btnAdd.addActionListener(this::actionPerformed);

            installMCButton = new JButton("Install MineCraft");
            installMCButton.addActionListener(this::actionPerformed);

            JPanel advancedFramePanel = new JPanel();

            advancedFramePanel.setLayout(new GridLayout(2 , 2));

            advancedFramePanel.add(mcLabel);
            /*advancedFramePanel.add(mcTextBox);*/

            advancedFramePanel.add(btnAdd);
            advancedFramePanel.add(installMCLabel);
            advancedFramePanel.add(installMCButton);

            advancedFrame.add(advancedFramePanel);

            advancedFrame.setSize(300, 150);
            advancedFrame.setVisible(true);

        }

        else if (e.getSource() == btnCancel) {

            System.exit(0);

        }

        else if (e.getSource() == btnAdd) {

            //potMcPath = Paths.get(mcTextBox.getText());

            JFileChooser mcPathChooser = new JFileChooser();

            mcPathChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int returnVal = mcPathChooser.showOpenDialog(null);

            if(returnVal == JFileChooser.APPROVE_OPTION) {

                System.out.println(mcPathChooser.getSelectedFile().getPath());
                potMcPath = Paths.get(mcPathChooser.getSelectedFile().getPath());
                JOptionPane.showMessageDialog(null, "Successfully added your Minecraft Path!", "Alert: OCD Installer",
                        JOptionPane.INFORMATION_MESSAGE);

            }

        }

        else if(e.getSource() == installMCButton){

            try {
                if(os.toLowerCase().contains("windows")) {

                    Runtime run = Runtime.getRuntime();
                    run.exec("rundll32 SHELL32.DLL,ShellExec_RunDLL msiexec /i dep" + File.separator + "MinecraftInstaller.msi REBOOT=ReallySuppress");

                }
                else if(os.toLowerCase().contains("mac")) {

                    Runtime run = Runtime.getRuntime();
                    run.exec(new String[] { "/usr/bin/open", "dep" + File.separator + "Minecraft.dmg"});

                }
                else if(os.toLowerCase().contains("linux")){

                    JOptionPane.showMessageDialog(null,
                            "Sorry this is not yet implemnted for Linux!",
                            "Error: OCD Installer", JOptionPane.ERROR_MESSAGE);

                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

    }
}
