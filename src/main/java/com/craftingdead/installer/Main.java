package com.craftingdead.installer;

import javax.swing.*;

public class Main {

	public static ImageIcon imgIcon = new ImageIcon(Main.class.getResource("/icon.png"));

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		Gui gui = new Gui();
		JFrame frame = new JFrame();
		frame.setTitle("OCD Installer");
		frame.setResizable(false);
		frame.setIconImage(imgIcon.getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(gui);
		frame.setSize(600, 450);
		frame.setVisible(true);
	}

}
